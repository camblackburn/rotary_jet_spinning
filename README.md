# rotary jet spinning

rotary jet spinning (RJS) method for producing medium-scale quanities of nanotextiles for filter mask material. 

## current projects & background
##### [Rotary Jet spinning](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3704151/) method developed by [Holly Mcllwee at Harvard Disease Biophysics Group](http://diseasebiophysics.seas.harvard.edu/research/nanotextiles/)


##### [Project 1000x1000](https://docs.google.com/document/d/1aACcbkyZjR7nZy_wWeGQ9U2iGy0y4QajBeOL8RqAZIQ/edit?pli=1#) developed at [Prakash Lab at Stanford](http://web.stanford.edu/group/prakash-lab/cgi-bin/labsite/publications/)
Characterization of N95 masks with SEM 
![](N95_SEM.jpg)

> TODO: test other filter materials in the SEM ...

#### FibeRio Cyclone L 1000 
- industrial laboratory machine for higher volume production of nanofibers 
- "Forcespinning" trademark technique 
- [Cyclone L 1000](https://www.prweb.com/releases/2010/11/prweb4740564.htm) release, but then no website for FibeRio or equipment - where did they go? 
- reported to produce 12,000 g/hr

